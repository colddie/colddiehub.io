---
layout: archive
title: "Publications"
permalink: /publications/
author_profile: true
---

{% if author.googlescholar %}
  You can also find my articles on <u><a href="{{author.googlescholar}}">my Google Scholar profile</a>.</u>
{% endif %}

{% include base_path %}


Publications
======
*	Cao W, Sun T, Kerckhofs G, Fardell G, Price B, and Dewulf W, “A simulation based study on the influence of the x-ray spectrum on the performance of multi-material beam hardening correction algorithms,” Measurement Science and Technology, accepted for publication, 2018.
*	Cao W, Sun T, Fardell G, Price B, and Dewulf W,“Comparative performance assessment of beam hardening correction algorithms applied on simulated datasets”, Journal of Microscopy, accepted for publication, 2018.
* Sun T, Clackdoyle R, Kim J, Fulton R, and Nuyts J, “Estimation of local data-insufficiency in motion-corrected helical CT,” IEEE Transaction on Radiation and Plasma Medicine Science, vol. 1, no. 4, pp. 346-357, July 2017.
*	Sun T, Kim JH, Fulton R, and Nuyts J, “An iterative projection-based motion estimation and compensation scheme for head X-ray CT,” Medical Physics, vol. 43, no. 10, pp. 5705–5716, 2016.
*	Kim JH, Sun T, Alcheikh A, Kuncic Z, Nuyts J, and R. Fulton, “Correction for human head motion in helical x-ray CT,” Physics in Medicine and Biology, vol. 61, no. 4, pp. 1416–1438, 2016.
*	Sun T, Wu TH, Wang SJ, Yang BH, Wu NY, and Mok GSP. “Low dose interpolated average CT for thoracic PET/CT attenuation correction using an active breathing controller”. Medical Physics, vol.40, pp. 102507, 2013.
*	Mok GSP, Sun T, Huang TC, and Vai MI. “Interpolated average CT for attenuation correction in PET – a simulation study”. IEEE Transaction on Biomedical Engineering, vol. 60, no. 7 pp. 1927-1934, 2013.
*	Sun T and Mok GSP, "Techniques for respiration-induced artifacts reductions in thoracic PET/CT". Quantitative Imaging in Medicine and Surgery, vol. 2, pp. 46-52, 2012.

  
Conference Proceedings and Presentations
======
*	Sun T, Kim JH, Fulton R, and Nuyts J. Data-driven correction for head motion in helical X-ray CT, in Conference Record of 4th International Meeting on image formation in X-ray CT (CT meeting), Bamberg, Germany, 2016. 
*	Sun T, Fulton R, Kim JH, and Nuyts J. Data driven helical CT rigid motion correction. In Conference Record of International Meeting on Fully Three-Dimensional Image Reconstruction in Radiology and Nuclear Medicine, Newport, USA, 2015. 
*	Kim JH, Sun T, Nuyts J, Kuncic Z, and Fulton R. Feasibility of correcting for realistic head motion in helical CT. In Conference Record of International Meeting on Fully Three-Dimensional Image Reconstruction in Radiology and Nuclear Medicine, Newport, USA, 2015. 
*	Sun T, Clackdoyle R, Fulton R, and Nuyts J. Quantification of local reconstruction accuracy for helical CT with motion correction. In Conference Record of IEEE Nuclear Science Symposium and Medical Imaging Conference, Seattle, USA, 2014. 
*	Ho CYT, Sun T, Wu TH, and Mok GSP. Performance evaluation of interpolated average CT for PET attenuation correction in different lesion characteristics. In Conference Record of IEEE Nuclear Science Symposium and Medical Imaging Conference, Seoul, Korea, 2013. 
*	Mok GSP, Ho C, and Sun T. Interpolated average CT for reducing cardiac PET/CT artifacts. In Conference Record of the Annual Congress of the European Association of Nuclear Medicine, Lyon, France, 2013. 
*	Mok GSP, Sun T, Wang SJ, and Wu TH. Clinical evaluation of interpolated average CT using active breathing controller (ABC) for attenuation correction in thoracic PET/CT. In Conference Record of the Society of Nuclear Medicine and Molecular Imaging 2013 Annual Meeting, Vancouver, Canada, 2013. 
*	Mok GSP, Ho C, and Sun T. Interpolated average CT for cardiac PET/CT attenuation correction - a simulation study. In Conference Record of the Society of Nuclear Medicine and Molecular Imaging 2013 Annual Meeting, Vancouver, Canada, 2013. 
*	Sun T, Wu TH, Wu NY, and Mok GSP. Low dose interpolated average CT for PET/CT attenuation correction using an active breathing controller (ABC). In Conference Record of IEEE Nuclear Science Symposium and Medical Imaging Conference, Anaheim, USA, 2012. 
*	Mok GSP, Sun T, Wu TH, Chang MB, and Huang TC. Interpolated average CT for attenuation correction in PET – a simulation study. In Conference Record of IEEE Nuclear Science Symposium and Medical Imaging Conference, Valencia, Spain, 2011. 


Abstracts, Poster Presentations
======
*	Sun T, Fulton R, Nuyts J. “A method to reduce data-redundancy artifacts for arbitrary source trajectories in CT imaging”, paper accepted for presentation in IEEE Nuclear Science Symposium and Medical Imaging Conference, Sydney, Australia, 2018.
*	Sun T, Fulton R, Nuyts J. Simultaneous Correction of motion and metal artifacts in head CT scanning, Poster in IEEE Nuclear Science Symposium and Medical Imaging Conference, Atlanta, USA, 2017. 
*	Sun T, Nuyts J, Fulton R. A motion compensation approach for dental cone-beam region-of-interest imaging, Poster in International Meeting on Fully Three-Dimensional Image Reconstruction in Radiology and Nuclear Medicine, Xi’an, China, 2017. 
*	Sun T, Clackdoyle R, Fulton R, and Nuyts J. Data-completeness prediction for reconstruction in the interior problem. Poster in IEEE Nuclear Science Symposium and Medical Imaging Conference, San Diego, USA, 2015. 


Patents
======
* A method and apparatus for motion correction in CT imaging 	US Patent Application, 15/608666, filed May 20 2017
* System and method for attenuation correction in emission computed tomography	US Patent Application, 14/210457, filed March 14 2014

Book Chapter
======
* Mok GSP, Sun T, and Liu C. Reducing respiratory artifacts in thoracic PET/CT. Book Chapter in Medical Imaging: Technology and Applications, CRC Press, October 1, 2013.

Thesis
======
* Sun T, Motion correction for head CT imaging, PhD thesis, 2018
* Sun T, Low Dose Interpolated Average CT for Thoracic PET/CT Attenuation Correction, Master thesis, 2014

<!--{% for post in site.publications reversed %}
  {% include archive-single.html %}
{% endfor %}-->
