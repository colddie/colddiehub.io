---
permalink: /
title: "About me"
excerpt: "About me"
author_profile: true
redirect_from: 
  - /about/
  - /about.html
---

Currently I am a postdoc research fellow in Massachusetts General Hospital and Harvard Medical School. My research topics involve techniques related to tomographic imaging modalities, e.g. CT, PET and MR. My research interests include image reconstruction, image processing and quantification.

Work experience
======
* Research Fellow in Department of Radiology, Massechussets General Hospital, Havard Medical School

Education
======
* Ph.D in Medical Imaging, KU Leuven, Belgium, 2018
* M.E. in Electrical and Electronics Engineering, University of Macau, 2014
* B.E. in Electronic Information Engineering, Tianjin University, 2010

Research interests
======
* Tomographic reconstruction
* CT artifacts reduction
* Novel CT imaging technique, spectral or phase contrast CT
* PET/CT and PET/MR motion correction

<!---
As a researcher, I always believe that the applications of the engineering could benefit clinical medicine. Specifically, I am interested in applying ways of software and hardware techniques into medical imaging field. At earlier stage of my research (master degree), I was involving in designing a framework of reducing the radiation dose and improving the image quality in PET/CT imaging. The framework involved hardware and software innovation, and has been proven superior than conventional routines in clinical trials. During my PhD, I have been involving in designing ways in improving image quality and patient comforts in CT imaging. The proposed work has been proved to provide images with significantly better quality than the ones produced form vendor softwares. The radiation dose and the total scanning time could be reduced in many cases. Some of my finished work has been already implemented in the local hospital regularly, and it is starting to make the clinical impact. These work also has the potential to be commercialized in the near future, as some company has demonstrated the interests.


 A data-driven personal website 
======

Getting started
======
1. Register a GitHub account if you don't have one and confirm your e-mail (required!)
1. Fork [this repository](https://github.com/academicpages/academicpages.github.io) by clicking the "fork" button in the top right. 
1. Go to the repository's settings (rightmost item in the tabs that start with "Code", should be below "Unwatch"). Rename the repository "[your GitHub username].github.io", which will also be your website's URL.
1. Set site-wide configuration and create content & metadata (see below -- also see [this set of diffs](http://archive.is/3TPas) showing what files were changed to set up [an example site](https://getorg-testacct.github.io) for a user with the username "getorg-testacct")
1. Upload any files (like PDFs, .zip files, etc.) to the files/ directory. They will appear at https://[your GitHub username].github.io/files/example.pdf.  
1. Check status by going to the repository settings, in the "GitHub pages" section

Create content & metadata
------
For site content, there is one markdown file for each type of content, which are stored in directories like _publications, _talks, _posts, _teaching, or _pages. For example, each talk is a markdown file in the [_talks directory](https://github.com/academicpages/academicpages.github.io/tree/master/_talks). At the top of each markdown file is structured data in YAML about the talk, which the theme will parse to do lots of cool stuff. The same structured data about a talk is used to generate the list of talks on the [Talks page](https://academicpages.github.io/talks), each [individual page](https://academicpages.github.io/talks/2012-03-01-talk-1) for specific talks, the talks section for the [CV page](https://academicpages.github.io/cv), and the [map of places you've given a talk](https://academicpages.github.io/talkmap.html) (if you run this [python file](https://github.com/academicpages/academicpages.github.io/blob/master/talkmap.py) or [Jupyter notebook](https://github.com/academicpages/academicpages.github.io/blob/master/talkmap.ipynb), which creates the HTML for the map based on the contents of the _talks directory).

**Markdown generator**
Example: editing a markdown file for a talk
![Editing a markdown file for a talk](/images/editing-talk.png)

For more info
------
More info about configuring academicpages can be found in [the guide](https://academicpages.github.io/markdown/). The [guides for the Minimal Mistakes theme](https://mmistakes.github.io/minimal-mistakes/docs/configuration/) (which this theme was forked from) might also be helpful. -->
